export function inflate(buffer, outlen) {
	var out = new ArrayBuffer(outlen);
	const nbits = buffer.byteLength * 8;

	function bit(n) {
		const byte = Math.floor(n / 8);
		n = 7 - (n % 8);
		return ((buffer[byte] & (1 << n)) >> n);
	}

	function bits(n, count) {
		var ret = 0;
		while (count-- > 0) {
			ret = (ret << 1) | bit(n++);
		}
		return ret;
	}

	function byte(n) {
		return buffer[Math.floor(n/8)];
	}

	console.log(buffer[0].toString(2));

	var i = 0;
	var o = 0;
	var lastBlock = 0;
	var blockType = 0;
	do {
		lastBlock = bit(i++);
		blockType = bits(i, 2);
		i += 2;

		if (blockType == 0) {
			i = i + (i % 8);
			var length = byte(i);
			console.log("stored block length " + length);
			i += 16;	// skip length and ones complement
			while (length-- > 0) {
				out[o++] = byte(i);
				i += 8;
			}
		} else {
			console.log("block is deflated\n");
			var endOfBlock = 0;
			while (!endOfBlock) {
				var literal = 0;
				if (literal < 256) {
					out[o++] = literal;
				} else {
					if (literal == endOfBlock) {
						break;
					}
					var distance = 0;
					i -= distance * 8;
					while (length-- > 0) {
						out[o++] = byte(i);
						i += 8;
					}
				}
			}
		}

	} while (false && lastBlock == 0);

	return out;
}
