import { inflate } from './inflate.js';

export class Zip {
	#view;
	#files;

	constructor(buffer) {
		this.#view = new DataView(buffer);
		this.#files = new Array();

		var hdr;
		var offset = 0;
		const length = this.#view.byteLength;
		while ((hdr = this.#findHeader(offset, length)) &&
			(hdr != null)) {
			this.#files.push(hdr);
			offset += 30 + hdr.nameLength +
				hdr.extraLength + hdr.compressed;
		}
	}

	decompress(file) {
		const hdr = this.#files[file];
		const start = hdr.offset + 30 + hdr.nameLength + hdr.extraLength;

		const buffer = new ArrayBuffer(hdr.compressed);
		for (var i = 0; i < hdr.compressed; i++) {
			buffer[i] = this.#u8(start + i);
		}

		switch (hdr.method) {
		case 0:		// STORED, no compression
			return this.#getString(start, hdr.compressed);

		case 8:		// DEFLATE
		case 9:		// DEFLATE64(tm)
			console.log("inflating " + hdr.name);
			return inflate(buffer, hdr.decompressed);

		default:
			console.log("unsupported compression algorithm");
		}
		return null;
	}

	getFiles() {
		return this.#files;
	}

	#u8(offset) {
		return this.#view.getUint8(offset);
	}

	#u16(offset) {
		return this.#view.getUint16(offset, true);
	}

	#u32(offset) {
		return this.#view.getUint32(offset, true);
	}

	#getString(offset, length) {
		var ret = ''
		for (var i = 0; i < length; i++) {
			ret += String.fromCharCode(this.#u8(offset + i));
		}
		return ret;
	}

	#findHeader(offset, length) {
		const hdrMagic = 0x04034b50;
		for (var hdr = offset; hdr < length - 4; hdr++) {
			if (this.#u32(hdr) == hdrMagic) {
				return {
					offset: hdr,
					version: this.#u16(hdr + 4),
					flags: this.#u16(hdr + 6),
					method: this.#u16(hdr + 8),
					time: this.#u16(hdr + 10),
					date: this.#u16(hdr + 12),
					crc32: this.#u32(hdr + 14),
					compressed: this.#u32(hdr + 18),
					uncompressed: this.#u32(hdr + 22),
					nameLength: this.#u16(hdr + 26),
					extraLength: this.#u16(hdr + 28),
					name: this.#getString(hdr + 30,
						this.#u16(hdr + 26)),
					extra: this.#getString(hdr + 30 +
						this.#u16(hdr + 26),
						this.#u16(hdr + 28)),
				};
			}
		}
		return null;
	}
}
