import { Epub } from './epub.js';

function loadFile(file) {
	const reader = new FileReader();
	reader.onload = function(contents) {
		const container = document.querySelector('#current-page');
		const epub = new Epub(contents.target.result, container);

		const prev = document.querySelector('#prev');
		const next = document.querySelector('#next');
		prev.onclick = function (e) {
			epub.back();
		}

		next.onclick = function (e) {
			epub.forward();
		}

		window.onkeydown = function (e) {
			switch (e.key) {
			case "ArrowRight":
			case "n":
				epub.forward();
				break;

			case "ArrowLeft":
			case "p":
				epub.back();
				break;
			}
		}
	}
	reader.readAsArrayBuffer(file);
}

function uploadFile() {
	loadFile(document.querySelector('#epub-file').files[0]);
}

function dragenter(e) {
	e.stopPropogation();
	e.preventDefault();
}

function dragover(e) {
	e.stopPropogation();
	e.preventDefault();
}

function drop(e) {
	e.stopPropogation();
	e.preventDefault();
	loadFile(e.dataTransfer.files[0]);
}

document.body.addEventListener("dragenter", dragenter, false);
document.body.addEventListener("dragover", dragover, false);
document.body.addEventListener("drop", drop, false);
document.querySelector('#epub-file').onchange = uploadFile;
