import { Zip } from './zip.js';

export class Epub {
	#current = 0;
	#files = {};
	#items = {};
	#last = 0;
	#container;

	constructor(buffer, container) {
		this.#container = container;

		const dom = new DOMParser();
		const zip = new Zip(buffer);
		const files = zip.getFiles();
		for (var file in files) {
			const name = files[file].name;
			const contents = zip.decompress(file);
			this.#files[name] = contents;
		}

		const metaContainer =
			dom.parseFromString(this.#files['META-INF/container.xml'],
				'text/xml');

		const rootfiles =
			metaContainer.getElementsByTagName('rootfile');

		const rootfileName =
			rootfiles[0].getAttribute('full-path');

		const rootfile =
			dom.parseFromString(this.#files[rootfileName],
				'text/xml');

		const items = rootfile.getElementsByTagName('item');

		for (var item in items) {
			try {
				const href = items[item].getAttribute('href');
				const id = items[item].getAttribute('id');
				const type = items[item].getAttribute('media-type');
				this.#items[id] = {
					file: href,
					type: type,
					order: -1,
				};
			} catch {}
		}

		const itemrefs = rootfile.getElementsByTagName('itemref');
		for (var ref in itemrefs) {
			try {
				const id = itemrefs[ref].getAttribute('idref');
				this.#items[id].order = ref;
				this.#last++;
			} catch {}
		}

		for (var item in this.#items) {
			if (this.#items[item].type.endsWith('xml')) {
				const file = this.#items[item].file;
				this.#files[file] = this.#adjustXml(file);
			}
		}

		this.jumpTo(this.#findIdByOrder(0));
	}

	#absolutePath(from, to) {
		if ((!to) || to.startsWith('http:') || to.startsWith('https:')
			|| to.startsWith('data')) {
				return null;
		}

		if (to.indexOf('/') == -1) {
			return to;
		}

		while (to[0] == '/') {
			to = to.substring(1);
		}

		const parts = to.split('/');
		var out = [];
		for (var part in parts) {
			if (parts[part] == '.' || parts[part] == '') {
				continue;
			} else if (parts[part] == '..') {
				out.pop();
			} else {
				out.push(parts[part]);
			}
		}
		return out.join('/');
	}

	#replace(element, file, attribute) {
		const src = this.#absolutePath(file,
				element.getAttribute(attribute));

		if (!src) {
			return;
		}

		var type = undefined;
		for (var item in this.#items) {
			if (this.#items[item].file == src) {
				type = this.#items[item].type;
			}
		}

		if (!type) {
			return;
		}

		const data = btoa(this.#files[src]);
		element.setAttribute(attribute, 'data:' + type + ';base64,' + data);
	}

	#replaceAll(file, xml, tag, attribute) {
		var elements = xml.getElementsByTagName(tag);
		for (var i = 0; i < elements.length; i++) {
			this.#replace(elements[i], file, attribute);
		}
	}

	#adjustXml(file) {
		var xml = new DOMParser().parseFromString(this.#files[file],
                                                'text/xml');
		this.#replaceAll(file, xml, 'img', 'src');
		this.#replaceAll(file, xml, 'image', 'xlink:href');
		this.#replaceAll(file, xml, 'link', 'href');

		var links = xml.getElementsByTagName('a');
		for (var i = 0; i < links.length; i++) {
			console.log(links[i]);
			var href = this.#absolutePath(file,
				links[i].getAttribute('href'));

			if (!href) {
				console.log("blank");
				links[i].target = "_blank";
			} else {
				console.log(href, this.#findIdByPath(href));
				const epub = this;
				links[i].onclick = function(e) {
					console.log("jumping to " + href);
					epub.jumpTo(this.#findIdByPath(href));
					return false;
				}
			}
		}
		return new XMLSerializer().serializeToString(xml);
	}

	#findIdByPath(path) {
		for (var i in this.#items) {
			if (this.#items[i].file == path) {
				return i;
			}
		}
	}

	#findIdByOrder(n) {
		for (var i in this.#items) {
			if (this.#items[i].order == n) {
				return i;
			}
		}
	}

	back() {
		this.#current--;
		if (this.#current < 0) {
			this.#current = 0;
			return;
		}
		this.jumpTo(this.#findIdByOrder(this.#current));
	}

	forward() {
		this.#current++;
		if (this.#current >= this.#last) {
			this.#current = this.#last - 1;
			return;
		}
		this.jumpTo(this.#findIdByOrder(this.#current));
	}

	jumpTo(id) {
		const file = this.#items[id].file;
		const type = this.#items[id].type;

		const container = this.#container;
		
		container.removeAttribute('srcdoc');
		container.removeAttribute('src');

		/*
		const doc = container.contentWindow.document;
		doc.open(type + ";charset=UTF-8");
		doc.write(this.#files[file]);
		doc.close();

		container.height = doc.body.scrollHeight + "px";

		doc.open();
		doc.close();
		*/

		container.src = "data:" + type +
			";charset=UTF-8;base64," + btoa(this.#files[file]);

		//container.contentWindow.focus();
	}
}
